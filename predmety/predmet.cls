\ProvidesClass{predmet}

% base class
\LoadClass[10pt, a4paper]{book}

% style
\usepackage[top=3cm,left=2cm,right=2cm,bottom=2.5cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage{titlesec, blindtext, color}
\usepackage[utf8]{inputenc}
\usepackage{enumitem}
\usepackage{helvet}

\usepackage{tikz}

% table
\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% chapter title style
\definecolor{gray75}{gray}{0.75}
\newcommand{\hsp}{\hspace{20pt}}
\titleformat{\chapter}[hang]{\Huge\bfseries}{\thechapter\hsp\textcolor{gray75}{|}\hsp}{0pt}{\Huge\bfseries}

% odsadenie
\setlength{\parindent}{0em}
\setlength{\parskip}{0.75em plus 0.25em minus 0.25em}

% math
\usepackage{amsmath,amssymb,trimclip,adjustbox}

% listings
\usepackage{listings}
\definecolor{mygray}{rgb}{0.7,0.7,0.7}
\lstset{
	frame=ltb,
	frameround=fftt,
	rulecolor=\color{mygray},
	basicstyle=\footnotesize\ttfamily,
	xleftmargin=1.5em,
	xrightmargin=0.5em,
	frameshape={YNN}{Y}{}{YNN}
}

% list
\setlist{
	itemsep=0.125em plus 0.125em minus 0.125em,
	parsep=0.25em plus 0.125em minus 0.125em,
	partopsep=0em,
	topsep=0em
}

\setlist[description] {
	topsep=0em
}

\usepackage{graphicx}
\usepackage{wrapfig}