# Státnice okruhy

## Štruktúra

Obsah repozitára:

* `spolecne_okruhy.tex` - skombinuje všetky predmety do jedného dokumentu,
* `spolecne_okruhy_sans.tex` - skombinuje všetky predmety do jedného dokumentu, používa písmo Sans Serif pre dobrú čitateľnosť na elektronických zariadeniach
* `predmety` - adresár obsahujúci jednotlivé okruhy
* `predmety/BI-*.tex` - samostatný dokument pre materiál a okruhy k danému predmetu

## Závislosti

### Ubuntu (16.04)

```shell
sudo apt install git texlive-latex-base texlive-fonts-recommended texlive-latex-extra
```

## Build

TODO:

```shell
git clone https://gitlab.fit.cvut.cz/kravemir/statnice-okruhy.git
cd statnice-okruhy
make all
```

Clean:

```shell
make clean
```

## Úprava

TODO: IDE napríklad Texmaker, Gummi