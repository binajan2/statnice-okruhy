
PREDMETY_TEX := $(wildcard predmety/*.tex)
PREDMETY_PDF := $(PREDMETY_TEX:%.tex=%.pdf)
PREDMETY_PDF_SANS := $(PREDMETY_TEX:%.tex=%.sans.pdf)

PDF_FILES=spolecne_okruhy.pdf spolecne_okruhy_sans.pdf $(PREDMETY_PDF) $(PREDMETY_PDF_SANS)


all: $(PDF_FILES) $(PREDMETY_PDF)

%.pdf: %.tex predmety/*.tex Makefile
	pdflatex $<
	pdflatex $<

predmety/%.pdf: predmety/%.tex Makefile
	cd predmety && pdflatex $*.tex
	cd predmety && pdflatex $*.tex
	rm predmety/$*.log predmety/$*.aux

predmety/%.sans.pdf: predmety/%.tex Makefile
	cd predmety && pdflatex -jobname=$*.sans '\renewcommand{\familydefault}{\sfdefault}\input $*.tex'
	cd predmety && pdflatex -jobname=$*.sans '\renewcommand{\familydefault}{\sfdefault}\input $*.tex'
	rm predmety/$*.sans.log predmety/$*.sans.aux

clean:
	rm -f $(PDF_FILES) $(PREDMETY_PDF) $(PREDMETY_PDF_SANS)
